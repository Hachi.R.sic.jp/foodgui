import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton sobaButton;
    private JButton somenButton;
    private JButton pastaButton;
    private JLabel Sub_Total_Money;
    private JLabel Ordered_Item;
    private JButton CheckOutButton;
    private JTextPane textPane2;
    private JTextPane textPane3;
    private JLabel Subtotal;
    private JLabel Title;
    private JLabel omoriinfo;
    private JLabel Tax;
    private JLabel Tax_Money;
    private JLabel Total;
    private JLabel Total_Money;
    private JButton resetButton;
    int TotalMoney=0;

int get_food_money(String food){
    int Money=0;
    switch(food){
        case "Tempra":
            Money=180;
            break;
        case "Ramen":
            Money=380;
            break;
        case "Udon":
            Money=240;
            break;
        case "Soba":
            Money=250;
            break;
        case "Somen":
            Money=220;
            break;
        case "Pasta":
            Money=320;
            break;
        case "Ramen(L)":
            Money=430;
            break;
        case "Udon(L)":
            Money=290;
            break;
        case "Soba(L)":
            Money=300;
            break;
        case "Somen(L)":
            Money=270;
            break;
        case "Pasta(L)":
            Money=370;
            break;
        default:
            Money=0;
            break;
    }
    return Money;
};
    int total_calculate(String food){
        TotalMoney+=get_food_money(food);
        return TotalMoney;
    }

    int OmoriYN(){
        int  confirmation = JOptionPane.showConfirmDialog(null,
                "\n" +
                        "Would you like to make a large serving? (+50yen)",
                "Omori Conformation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0) {
            textPane1.setText("Omori received");
            return 0;
        }
        else {
            textPane1.setText("No Omori received");
            return 1;
        }
    }
    void order(String food){
        int  confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Conformation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            Sub_Total_Money.setText(total_calculate(food)+" yen");//sub合計金額の表示
            Tax_Money.setText((int)(TotalMoney*0.1)+" yen");//税金の表示
            Total_Money.setText(TotalMoney+(int)(TotalMoney*0.1)+" yen");//合計金額の表示
            String currentText = textPane2.getText();
            textPane2.setText(currentText + food + "\n");
            String currentText2 = textPane3.getText();
            if(get_food_money(food)<1000) {
                textPane3.setText(currentText2 + "   " + get_food_money(food) + "yen" + "\n");
            }
            else{
                textPane3.setText(currentText2 + " "  + get_food_money(food) + "yen" + "\n");
            }
            textPane1.setText("Order for " + food + " received.");
            JOptionPane.showMessageDialog(null,
                    "Order for " + food + " received.");//受領のボックス表示
            //textPane2.setText(food);//foodの表示
            //textPane3.setText(""+get_food_money(food));//foodの金額の表示
        }
        else if(confirmation == 1){
            textPane1.setText("No order received");
        }
    }

    public foodGUI() {
        Title.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        textPane1.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 15));
        textPane2.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 30));
        textPane3.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 30));
        Sub_Total_Money.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        Subtotal.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        Tax.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        Total.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 30));
        Tax_Money.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        Total_Money.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 30));
        Ordered_Item.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 30));
        tempuraButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        ramenButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        udonButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        sobaButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        somenButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        pastaButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 50));
        CheckOutButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 40));
        resetButton.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        omoriinfo.setFont(new Font("ＭＳ Pゴシック", Font.BOLD, 20));
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tempura.jpg")
        ));
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Soba.jpg")
        ));
        somenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Somen.jpg")
        ));
        pastaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Pasta.jpg")
        ));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempra");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (OmoriYN() == 0) {
                    order("Ramen(L)");
                }
                else{
                    order("Ramen");
                }
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (OmoriYN() == 0) {
                    order("Udon(L)");
                }
                else{
                    order("Udon");
                }
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (OmoriYN() == 0) {
                    order("Soba(L)");
                }
                else{
                    order("Soba");
                }
            }
        });
        somenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (OmoriYN() == 0) {
                    order("Somen(L)");
                }
                else{
                    order("Somen");
                }
            }
        });
        pastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (OmoriYN() == 0) {
                    order("Pasta(L)");
                }
                else{
                    order("Pasta");
                }
            }
        });
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int  confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Conformation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + (TotalMoney+(int)(TotalMoney*0.1)) + " yen");//受領のボックス表示
                    TotalMoney=0;
                    textPane1.setText("");
                    textPane2.setText("");
                    textPane3.setText("");
                    Sub_Total_Money.setText("0 yen");
                    Total_Money.setText("0 yen");
                    Tax_Money.setText("0 yen");
                }
                else if(confirmation == 1){
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int  confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you want to reset the Order Items?",
                        "Reset Conformation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0) {
                    TotalMoney=0;
                    textPane1.setText("");
                    textPane2.setText("");
                    textPane3.setText("");
                    Sub_Total_Money.setText("0 yen");
                    Total_Money.setText("0 yen");
                    Tax_Money.setText("0 yen");
                    JOptionPane.showMessageDialog(null,
                            "The Order Items are cleared.");//受領のボックス表示
                }
                else if(confirmation == 1){
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
